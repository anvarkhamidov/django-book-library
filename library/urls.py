from django.urls import path
from . import views

from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    path('', views.books_list, name='index'),
    path('bt', views.bulmatest, name='bulma')
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)