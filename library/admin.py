from django.contrib import admin
from library.models import Book, Category

admin.site.register(Book)
admin.site.register(Category)
