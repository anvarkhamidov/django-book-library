from django.shortcuts import render
from library.models import Book, Category


def books_list(request):
    books = Book.objects.all()
    cats = Category.objects.all()
    return render(request, 'library/books_list.html', {'books': books, 'cats': cats})

def bulmatest(request):
    return render(request, 'library/bulmatest.html', {})