
from django.db import models


class Book(models.Model):
    title = models.CharField(max_length=255)
    description = models.TextField()
    cover = models.ImageField(blank=False, null=True)
    author = models.CharField(max_length=255)
    category = models.ForeignKey('library.Category', on_delete=models.CASCADE)
    created_date = models.DateTimeField(auto_created=True, auto_now_add=True)

    def __str__(self):
        return self.title


class Category(models.Model):
    name = models.CharField(max_length=255)

    def __str__(self):
        return self.name
